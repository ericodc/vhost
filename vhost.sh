#!/bin/bash
#==================================================================#
# Autor:    Diogo Alexsander Cavilha <diogocavilha@gmail.com>
# Data:     06/05/2014
#
# Descrição:
#   Cria a configuração de virtual host.
#   Gera o link simbólico para /etc/apache2/sites-enabled
#   Adiciona IP e Server name no arquivo /etc/hosts
#   Reinicia o servidor Apache
#==================================================================#

#. config.ini
VERSION="2.0"
HOSTS_FILE="/etc/hosts"
SITES_AVAILABLE="/etc/apache2/sites-available"
SITES_ENABLED="/etc/apache2/sites-enabled"
IP_ADDRESS="127.0.0.1"
SERVER_NAME="Apache"

#. messages
messageRed()
{
    echo -e "\033[0;31m$1\033[0m"
    sleep 1
}

messageYellow()
{
    echo -e "\033[1;33m$1\033[0m"
}

messageBlue()
{
    echo -e "\033[0;34m$1\033[0m"
    sleep 1
}

messageGreen()
{
    echo -e "\033[0;32m$1\033[0m"
    sleep 1
}

environmentVars=""

#------------------------------------------------------------------#
# This function checks whether or not the user who is running this
# script is root.
#------------------------------------------------------------------#
requireRoot()
{
    if [ ! "$USER" == "root" ]; then
        messageRed "\nVocê precisa estar logado como root.\n"
        exit 1
    fi
}

#------------------------------------------------------------------#
# This function receives a file name in order to check whether or
# not it exists.
# Returned value:
#   0 - File does not exist.
#   1 - File exists.
#------------------------------------------------------------------#
fileExists()
{
    if [ -e "$1" ]; then
        return 1
    else
        return 0
    fi
}

#------------------------------------------------------------------#
# This function receives a directory name in order to check whether
# or not it exists.
# Returned value:
#   0 - Diretory does not exist.
#   1 - Diretory exists.
#------------------------------------------------------------------#
folderExists()
{
    if [ -d "$1" ]; then
        return 1
    else
        return 0
    fi
}

#------------------------------------------------------------------#
# This function receives a port number in order to check whether
# or not it is available.
# Returned value:
#   0 - This port number is available.
#   1 - This port number is not available.
#------------------------------------------------------------------#
isUsedPort()
{
    $(nc -z -w5 $IP_ADDRESS $1)
    if [ $? -eq 0 ]; then
        return 1
    else
        return 0
    fi
}

#------------------------------------------------------------------#
# This function requests the environment variable name.
#------------------------------------------------------------------#
requireEnvironmentVarName()
{
    echo -n " Nome: "
    read evName
    if [ -z $evName ]; then
        requireEnvironmentVarName
    fi
}

#------------------------------------------------------------------#
# This function requests the environment variable value.
#------------------------------------------------------------------#
requireEnvironmentVarValue()
{
    echo -n " Valor: "
    read evValue
    if [ -z $evValue ]; then
        requireEnvironmentVarValue
    fi
}

#------------------------------------------------------------------#
# This function joins all environment variables to a single string.
#------------------------------------------------------------------#
appendEnvironmentVars()
{

    requireEnvironmentVarName
    requireEnvironmentVarValue

    environmentVars+="\tSetEnv $evName \"$evValue\"\n"

    echo -e -n "\n Adicionar mais uma? [S/N]: "
    read oneMore
    case $oneMore in
        "s"|"S") appendEnvironmentVars;;
            *) ;;
    esac
}

#------------------------------------------------------------------#
# This function requests the file name that will be created
# in order to store the configuration code.
#------------------------------------------------------------------#
requireFileName()
{
    echo -n -e "\n Nome do arquivo que conterá a configuração do host: "
    read vhFileName

    if [ ! -z $vhFileName ]; then
        vhFileName+=".conf"

        $(fileExists "$SITES_AVAILABLE/$vhFileName")

        if [ $? -eq 1 ]; then
            messageRed "\n [$SITES_AVAILABLE/$vhFileName] Arquivo já existe.\n"
            requireFileName
        fi
    else
        requireFileName
    fi
}

#------------------------------------------------------------------#
# This function requests the application directory.
#------------------------------------------------------------------#
requireApplicationFolder()
{
    echo -n " Caminho da aplicação (Ex: /diretorio/contendo/aplicacao): "
    read vhPath

    if [ ! -z $vhPath ]; then
        $(folderExists "$vhPath")

        if [ $? -eq 0 ]; then
            messageRed "\n [$vhPath] O diretório não existe.\n"
            requireApplicationFolder
        fi
    else
        requireApplicationFolder
    fi
}

#------------------------------------------------------------------#
# This function requires the port number on which the
# application will run.
#------------------------------------------------------------------#
requireApplicationPort()
{
    echo -n " Porta: "
    read vhPort

    if [ ! -z $vhPort ]; then
        $(isUsedPort "$vhPort")

        if [ $? -eq 1 ]; then
            messageRed "\n [$vhPort] Esta porta já está sendo utilizada.\n"
            requireApplicationPort
        fi
    fi
}

#------------------------------------------------------------------#
# This function requires the application's server name.
#------------------------------------------------------------------#
requireApplicationServerName()
{
    echo -n " Server Name: "
    read vhServerName

    if [ -z $vhServerName ]; then
        requireApplicationServerName
    fi
}

#------------------------------------------------------------------#
# This function writes the host configuration code on its
# correspondent file.
#------------------------------------------------------------------#
writeConfiguration()
{
    vhPort=$1
    vhServerName=$2

    if [ -z $vhPort ]; then
        `echo "<VirtualHost *:80>" >> $SITES_AVAILABLE/$vhFileName`
    else
        `echo "Listen $vhPort" >> $SITES_AVAILABLE/$vhFileName`
        `echo "<VirtualHost *:$vhPort>" >> $SITES_AVAILABLE/$vhFileName`
    fi

    `echo -e "\tDocumentRoot $vhPath" >> $SITES_AVAILABLE/$vhFileName`

    if [ ! -z $vhServerName ]; then
        `echo -e "\tServerName $vhServerName" >> $SITES_AVAILABLE/$vhFileName`
        `echo "" >> $SITES_AVAILABLE/$vhFileName`
    else
        `echo -e "\t#ServerName $vhFileName.local" >> $SITES_AVAILABLE/$vhFileName`
    fi

    if [ ${#environmentVars} -gt 0 ]; then
        `echo -e $environmentVars >> $SITES_AVAILABLE/$vhFileName`
    else
        `echo -e "\t#SetEnv VAR_NAME\t\"value\"" >> $SITES_AVAILABLE/$vhFileName`
    fi

    `echo -e "\t<Directory $vhPath/>" >> $SITES_AVAILABLE/$vhFileName`
    `echo -e "\t\tOptions Indexes FollowSymLinks MultiViews" >> $SITES_AVAILABLE/$vhFileName`
    `echo -e "\t\tAllowOverride All" >> $SITES_AVAILABLE/$vhFileName`
    `echo -e "\t\tOrder allow,deny" >> $SITES_AVAILABLE/$vhFileName`
    `echo -e "\t\tallow from all" >> $SITES_AVAILABLE/$vhFileName`
    `echo -e "\t</Directory>" >> $SITES_AVAILABLE/$vhFileName`
    `echo "</VirtualHost>" >> $SITES_AVAILABLE/$vhFileName`
}

restartApache()
{
    messageBlue " * Reiniciando o apache..."
    service apache2 restart
}

#------------------------------------------------------------------#
# This function creates the files within sites-enabled and
# sites-available folders.
#------------------------------------------------------------------#
addHost()
{
    requireRoot
    requireFileName
    requireApplicationFolder
    requireApplicationPort

    if [ ${#vhPort} -eq 0 ]; then
        requireApplicationServerName
    fi

    echo -e -n "\n Deseja adicionar variáveis de ambiente? [S/N]: "
    read addEnvironmentVar
    case $addEnvironmentVar in
        "s"|"S") appendEnvironmentVars;;
            *) ;;
    esac

    messageBlue "\n * Criando host..."
    touch $SITES_AVAILABLE/$vhFileName

    writeConfiguration "$vhPort" "$vhServerName"

    messageBlue " * Habilitando host..."
    a2ensite $vhFileName

    if [ ${#vhServerName} -gt 0 ]; then
        messageBlue "\n * Fazendo backup do arquivo $HOSTS_FILE"

        cp /etc/hosts /etc/hosts.backup

        messageBlue " * Adicionando entrada no arquivo $HOSTS_FILE"
        `echo -e "$IP_ADDRESS $vhServerName" >> $HOSTS_FILE`
    fi

    restartApache
    address="http://"

    if [ ${#vhPort} -gt 0 ]; then
        address+="$IP_ADDRESS:$vhPort"
    else
        if [ ${#vhServerName} -gt 0 ]; then
            address+="$vhServerName"
        fi
    fi

    messageBlue " Host criado com sucesso --> [$address]"
}

#------------------------------------------------------------------#
# This function receives the file name and remove it from
# sites-enabled and sites-available folders.
#------------------------------------------------------------------#
removeHost()
{
    requireRoot

    host=$1
    exists=0

    serverName=$(grep ServerName $SITES_ENABLED/$host | awk '{print $2}')
    documentRoot=$(grep DocumentRoot $SITES_ENABLED/$host | awk '{print $2}')

    messageBlue "\n * Removendo entrada do arquivo /etc/hosts"
    sed  "s/$IP_ADDRESS $serverName//" /etc/hosts > /etc/hosts2
    mv -f /etc/hosts2 /etc/hosts

    if [ ${#host} -gt 0 ]; then
        if [ -e "$SITES_ENABLED/$host" ]; then
            messageBlue " * Removendo link..."
            rm "$SITES_ENABLED/$host"

            exists=1
        fi

        if [ -e "$SITES_AVAILABLE/$host" ]; then
            messageBlue " * Removendo host..."
            rm "$SITES_AVAILABLE/$host"

            exists=1
        fi

        if [ $exists -eq 0 ]; then
            messageRed "\n Este host não existe.\n"
        else
            restartApache
        fi
    else
        messageRed "\n Host não informado."
    fi
}

#------------------------------------------------------------------#
# This function shows an available and enabled hosts list.
#------------------------------------------------------------------#
listHosts()
{
    echo ""
    messageYellow "[ $SITES_AVAILABLE ]\n"
    for hostName in $(ls $SITES_AVAILABLE); do
        echo -e "\t$SITES_AVAILABLE/\033[1;33m$hostName\033[0m"
    done

    echo ""
    messageYellow "[ $SITES_ENABLED ]\n"
    for hostName in $(ls $SITES_ENABLED); do
        echo -e "\t$SITES_AVAILABLE/\033[1;33m$hostName\033[0m"
    done

    echo ""
}

#------------------------------------------------------------------#
# This function shows the script version.
#------------------------------------------------------------------#
showVersion()
{
    echo -e VirtualHostAutomat v.$VERSION"\n"
}

#------------------------------------------------------------------#
# This function shows the script help.
#------------------------------------------------------------------#
helpScript()
{
    echo "Modo de usar: vhost [opções]"
    echo ""
    echo "Opções:"
    echo -e "  -a, --add\t\t\t                 Adiciona/Cria um host de forma interativa."
    echo -e "  -d, -r, --delete, --remove <nome do host>\t Deleta um host."
    echo -e "  -l, --list\t\t\t                 Exibe uma lista dos hosts criados."
    echo -e "  -h, --help\t\t\t                 Exibe este help ;)"
    echo -e "  -v, --version\t\t\t              Exibe a versão do script."
    echo ""
}

#------------------------------------------------------------------#
# This function manages this script call.
#------------------------------------------------------------------#
case $1 in
    "-a"|"--add") addHost;;
    "-d"|"-r"|"--delete"|"--remove") removeHost "$2";;
    "-l"|"--list") listHosts;;
    "-h"|"--help") helpScript;;
    "-v"|"--version") showVersion;;
esac